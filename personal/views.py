from django.shortcuts import render
from django.http import HttpResponse

def index(request):
   return render(request, 'personal/header.html')

def contact(request):
    return render(request, 'personal/contact.html',{'content':['You can reach me at ','ellemalixi@gmail.com or 09054582068']})
