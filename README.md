# BLOG
### this is basically my own site with blog
### the design can be better so please bare with me

## Built With
#### Django - web framework used
#### Python - language used

## Author
#### Michelle Malixi

## Acknowledgements
#### - thanks to all the code inspirations I got
#### - Hats off to anyone whose code was used
